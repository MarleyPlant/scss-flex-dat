<div align='center'>
    <h1><b>SCSS-Flex-Dat</b></h1>
    <p>Library for simplifying commonly used flexbox positionings into single lines using SCSS mixins.</p>

</div>

---

## 🧑‍💻 **USAGE**
`@include flex-dat($position, $flow, $second-row);`

`$position` - A two-part, space separated string which describes the flex layout using flex-dat shorthand.


`$flow` - A two-part, space separated string which describes either property controlled by flex-flow, being flex-direction and flex-wrap.


`$second-row` - A single string used to describe the align-content property.

### 1
```css
.selector {
    @include flex-dat('center');
}
```
Will add the following css to the selector.
```css
display: flex;
justify-content: center;
vertical-align: center;
flex-direction: column;
```

this will create a element that is centered vertically and horizontally


### 2

```css
display: flex;
flex-direction: row;
justify-content: space-between;
```
all of this code can be replaced with a single line.

`@include flex-dat('space-between');`


<br />



---

## 💻 **TECHNOLOGIES**

![CSS3](https://img.shields.io/badge/css3-%231572B6.svg?style=for-the-badge&logo=css3&logoColor=white)
![SASS](https://img.shields.io/badge/SASS-hotpink.svg?style=for-the-badge&logo=SASS&logoColor=white)

<br />

---

## 📌 **LINKS**

[<img alt="Gitlab" src="https://img.shields.io/badge/MarleyPlant-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white" />](https://gitlab.com/MarleyPlant)

<br />
